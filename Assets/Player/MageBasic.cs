﻿using UnityEngine;
using System.Collections;

public class MageBasic : PlayerBasic {

	public void MageClass(){
		CharacterClassName = "Mago";
		CharacterClassDescription = "YOU SHALL NOT PASS!";
		Mana = 50;
		Life = 30;
		Range = 10;
	}

}
