﻿using UnityEngine;
using System.Collections;

public class PlayerBasic : MonoBehaviour {

	private string characterClassName;
	private string characterClassDescription;

	private int mana;
	private int life;
	private int range;

	public string CharacterClassName{
		get{
			return characterClassName;
		}

		set{
			characterClassName = value;
		}
	}

	public string CharacterClassDescription{
		get{
			return characterClassDescription;
		}

		set{
			characterClassDescription = value;
		}
	}

	public int Mana{
		get{
			return mana;
		}

		set{
			mana = value;
		}
	}

	public int Life{
		get{
			return life;
		}

		set{
			life = value;
		}
	}

	public int Range{
		get{
			return range;
		}

		set{
			range = value;
		}
	}
}
