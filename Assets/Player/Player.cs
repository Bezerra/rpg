﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Player : PlayerBasic {

    public Text text;

    Rigidbody rigidbody;
    Vector3 velocity;
    
    void Start () {
        rigidbody = GetComponent<Rigidbody> ();
        //text.text = "";
    }

    void Update () {
        velocity = new Vector3 (Input.GetAxisRaw ("Horizontal"), 0, Input.GetAxisRaw ("Vertical")).normalized * 10;
    }

    void FixedUpdate() {
		rigidbody.MovePosition (rigidbody.position + velocity * Time.fixedDeltaTime);
    }

    void OnTriggerEnter(Collider other) 
    {
        if (other.gameObject.CompareTag ("Collectable"))
        {
            other.gameObject.SetActive (false);
        }
    }

}