﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public class ButtonSaveFile : MonoBehaviour {
	
	// Update is called once per frame
	public void onClick (int nextScene) {

		string path = Application.dataPath;
		Debug.Log(path);
		System.IO.StreamWriter file = new System.IO.StreamWriter(path + "\\changeScene.txt");
		file.WriteLine(1);

		file.Close();
		Application.LoadLevel(nextScene);
	}

	public void clickPlay (int MasterScene) {

		Application.LoadLevel(MasterScene);
	}

	public void clickQuit () {

		string path = Application.dataPath;
		Debug.Log(path);
		System.IO.File.Delete(path + "\\changeScene.txt");
		System.IO.File.Delete(path + "\\itemsLocation.txt");
		Application.Quit();
	}
}
