﻿using UnityEngine;
using System.Collections;
 public class GridManager : MonoBehaviour
{
    public int Width = 100;
    public int Height = 60;
 
    public GameObject hexPrefab;
    float xOffset = 0.892f;
    float zOffset = 0.804f;

    void Start(){

    	for (int x = 0; x < Width; x++ ){
    		for (int y = 0; y < Height; y++){

    			float xPos = x * xOffset;
    			if (y % 2 == 1){
    				xPos += xOffset/2f;
    			}

    			GameObject hex_go = (GameObject) Instantiate(hexPrefab, new Vector3(xPos,0,y*zOffset), Quaternion.identity);
    			hex_go.name = "Hex_" + x + "_" + y;
    			hex_go.transform.SetParent(this.transform);
    		}
    	}
    }

    void Update(){

    }

 /*
    public void GenerateGrid()
    {
        float inradius = (float)(0.5 * Mathf.Sqrt(3) * HexSideLength);
	float spaceBetweenTilesHorizontal = 2.0f * inradius;
	float spaceBetweenTilesVertical = 1.5f * HexSideLength;
 
	if (hex != null)
        {
            for (int x = 0; x < Width; x++)
            {
                for (int y = 0; y < Height; y++)
                {
                    GameObject tile = (GameObject)Instantiate(hex, new Vector3.zero, Quaternion.identity);
                    tile.transform.parent = gameObject.transform;
                    tile.transform.localPosition = new Vector3(x * spaceBetweenTilesHorizontal + (y & 1) * inradius, 0, y * spaceBetweenTilesVertical);
	        }
	    }
        }
 
        // Center the Grid
        gameObject.transform.Translate(new Vector3(-spaceBetweenTilesHorizontal * Width / 2.0f + inradius, 0, -spaceBetweenTilesVertical * Height / 2.0f - HexSideLength));
    }
    */
}