﻿using UnityEngine;
using System.Collections;

public class mouseDragAndDrop : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		Ray ray = Camera.main.ScreenPointToRay( Input.mousePosition );

		RaycastHit hitInfo;

		if( Physics.Raycast(ray, out hitInfo) ) {
			GameObject ourHitObject = hitInfo.collider.transform.parent.gameObject;

			// So...what kind of object are we over?
			if(ourHitObject.GetComponent<Hex>() != null) {
				// Ah! We are over a hex!
				//MouseOver_Hex(ourHitObject);
				//Debug.Log("Clicked On: " + ourHitObject.name);
			}

		}
	
	}
}
