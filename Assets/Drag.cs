﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Drag : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler {
	
	Vector3 screenPoint;
	Vector3 offset;

	public void OnBeginDrag(PointerEventData eventData)
	{
		screenPoint = Camera.main.WorldToScreenPoint(gameObject.transform.position);
		offset = gameObject.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z));
	}

	public void OnDrag(PointerEventData eventData)
	{

		Vector3 curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);
		Vector3 curPosition   = Camera.main.ScreenToWorldPoint(curScreenPoint) + offset;
		transform.position = curPosition;
	}

	public void OnEndDrag(PointerEventData eventData)
	{
		Debug.Log(gameObject.name + " " + gameObject.transform.position);
		Ray ray = Camera.main.ScreenPointToRay( Input.mousePosition );

		RaycastHit hitInfo;

		if( Physics.Raycast(ray, out hitInfo) ) {
			GameObject ourHitObject = hitInfo.collider.transform.parent.gameObject;
			Debug.Log(ourHitObject.transform.position);

			string path = Application.dataPath;
	        System.IO.StreamWriter file = new System.IO.StreamWriter(path+"\\itemsLocation.txt", true);
			file.WriteLine(ourHitObject.transform.position + " " + gameObject.name);

			file.Close();
		}
	}
}