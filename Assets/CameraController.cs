﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {

    public GameObject Cube;

    private Vector3 offset;

    void Start ()
    {
        offset = transform.position - Cube.transform.position;
    }
    
    void LateUpdate ()
    {
        transform.position = Cube.transform.position + offset;
    }
}