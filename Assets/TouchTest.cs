﻿using UnityEngine;
using System.Collections;

public class TouchTest : MonoBehaviour 
{
	public int nbTouches;

	void Start()
	{
		nbTouches = Input.touchCount;
	}

    void Update () 
    {
        //Debug.Log(nbTouches + " touch(es) detected");
        if(nbTouches > 0)
        {
            //Debug.Log(nbTouches + " touch(es) detected");

            for (int i = 0; i < nbTouches; i++)
            {
                Touch touch = Input.GetTouch(i);

                //Debug.Log("Touch index " + touch.fingerId + " detected at position " + touch.position);
            }
        }
}
}